//written by nathan brinda
#ifndef UTILS_H
#define UTILS_H
#include "Tree.h"
#define true 1
#define false 0

void process_text(FILE* fp, Tree* abtree, char* which_tree);
void interpreter(FILE* fp, Tree* abtree, char* tree_type);


#endif
