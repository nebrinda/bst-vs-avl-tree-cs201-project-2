//written by nathan brinda
#ifndef TREE_H
#define TREE_H
#include "BST.h"
#include "AVL.h"
#include "treeNode.h"

#define true 1
#define false  0

typedef struct Tree{

   struct BST* bst;
   AVL* avl;

} Tree;

Tree* initializeTree();
treeNode* search(treeNode* top_of_bst, char* value_to_delete);
treeNode* insert(treeNode* top_of_bst, char* value, Tree* abtree);
treeNode* delete(treeNode* top_of_bst, char* value_to_delete, Tree* abtree);
int isLeftChild(treeNode* parent, treeNode* insertedNode);

#endif