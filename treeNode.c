//written by nathan brinda
#include "treeNode.h"
#include <stdlib.h>

treeNode* newTreeNode(){
   treeNode* node = malloc(sizeof(treeNode));
   node->left = NULL;
   node->right = NULL;
   node->parent = NULL;
   node->value = "";
   node->frequency_of_word = 0;
   node->left_height = 0;
   node->right_height = 0;
   node->height = 0;
   return node;
}