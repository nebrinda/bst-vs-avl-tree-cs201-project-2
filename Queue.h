//written by nathan brinda
#ifndef QUEUE_H
#define QUEUE_H
#include "Node.h"

typedef struct Queue{

   int number_of_elements;
   Node* bottom;
   Node* top;
   

}Queue;

void enqueue(Queue* my_queue, treeNode* value);
Node* dequeue(Queue* my_queue);
Queue* initializeQueue();

#endif