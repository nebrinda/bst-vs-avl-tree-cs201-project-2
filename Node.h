//written by nathan brinda
#ifndef NODE_H
#define NODE_H
#include "treeNode.h"

typedef struct Node{

   treeNode* node_value;
   struct Node* next;
   struct Node* prev;

}Node;

Node* new_node();
#endif