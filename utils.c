//written by nathan brinda

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "scanner.h"
#include "utils.h"
#include "Tree.h"
#include "Queue.h"


static char* filter_ascii(char* token);
static void insertCorrectTree(Tree* abtree, char* tree_type, char* filtered);
static treeNode* deleteCorrectTree(Tree* abtree, char* tree_type, char* filtered);
static char* getTokenOrString(FILE* fp);
static void breadthFirstTraversal(Tree* abtree, char* report_or_show);
//static void printNode(treeNode* temp, Tree* abtree);
//static void printParentStatistics(treeNode* temp, Tree* abtree);
static int isThisAscii(int character);
static char* compress_spaces(char*token);

static void printNode(treeNode* temp, Tree* abtree,FILE* fp);
static void printParentStatistics(treeNode* temp, Tree* abtree,FILE* fp);

void process_text(FILE* fp, Tree* abtree, char* which_tree){

   if (fp == NULL){
      fprintf(stderr, "Error opening file");
      exit(0);
   }


   char* token = "";
   char* filtered = "";

   if (stringPending(fp)){ 
      token = readString(fp);
   }
   else
      token = readToken(fp);

   while (!feof(fp)){
      filtered = filter_ascii(token);
      if (strlen(filtered) != 0){
         insertCorrectTree(abtree, which_tree, filtered);       
      }
      if (stringPending(fp)){
         token = readString(fp);
      }
      else
         token = readToken(fp);
   }
   fclose(fp);
}

//compresses multiple spaces into a single space
static char* compress_spaces(char*token){
   int consecutive_space = false;
   int length = 0;
   char* final_token = malloc(sizeof(token) + 1);
   final_token = token;

   for (size_t i = 0; i < strlen(token); i++){

      if (isspace(token[i]) && consecutive_space == false){//first space found, add it
         final_token[length] = ' ';
         length++;
         consecutive_space = true;
      }
      else if (!isspace(token[i])){
         final_token[length] = token[i];
         length++;
         consecutive_space = false;
      }
   }
   final_token[length] = '\0';
   return final_token;

}

//takes in a word/phrase, and filters anything not an ascii letter
static char* filter_ascii(char* token){
   int number_of_letters = 0;
   int token_length = strlen(token);
   char* temp = malloc(token_length + 1);


   for (size_t i = 0; i < strlen(token); i++){
      if (!isThisAscii(token[i])){
         continue;
      }
      else if (isupper(token[i])){
         temp[number_of_letters] = tolower(token[i]);
         number_of_letters++;
      }
      else if (islower(token[i])){
         temp[number_of_letters] = tolower(token[i]);
         number_of_letters++;
   }
      else if (isspace(token[i])){
         temp[number_of_letters] = ' ';
         number_of_letters++;
      }
   }
   temp[number_of_letters] = '\0';
   temp = compress_spaces(temp);

   return temp;
}

static void insertCorrectTree(Tree* abtree, char* tree_type, char* filtered){

   if (tree_type[1] == 'a'){
      if (abtree->avl->top == NULL){
         abtree->avl->top = insert(abtree->avl->top, filtered, abtree);
         abtree->avl->number_of_nodes++;
      }
      else
         insert(abtree->avl->top, filtered, abtree);


   }
   else{
      if (abtree->bst->top == NULL){
         abtree->bst->top = insert(abtree->bst->top, filtered, abtree);
         abtree->bst->number_of_nodes++;
      }
      else
         insert(abtree->bst->top, filtered, abtree);

   }
}


static treeNode* deleteCorrectTree(Tree* abtree, char* tree_type, char* filtered){
   if (tree_type[1] == 'a')
      return delete(abtree->avl->top, filtered, abtree);
   else
      return delete(abtree->bst->top, filtered, abtree);
}


void interpreter(FILE* fp, Tree* abtree, char* tree_type){



   if (fp == NULL){
      fprintf(stderr, "Error opening file");
      exit(0);
   }

   char* command = "";
   char* word = "";
   int string_length = 0;
   treeNode* node = newTreeNode(0);

   command = getTokenOrString(fp);
   while (!feof(fp)){

      if (command[0] == 'i'){
         word = getTokenOrString(fp);
         word = filter_ascii(word);
         string_length = strlen(word);
         if (string_length != 0)
            insertCorrectTree(abtree, tree_type, word);
      }
      else if (command[0] == 'd'){
         word = getTokenOrString(fp);
         word = filter_ascii(word);
         string_length = strlen(word);
         if (string_length != 0)
            node = deleteCorrectTree(abtree, tree_type, word);
         if (string_length == 0 || node == NULL)
            printf("\"%s\"is not in the tree\n", word);
      }

      else if (command[0] == 'f'){
         word = getTokenOrString(fp);
         word = filter_ascii(word);
         string_length = strlen(word);
         if (string_length != 0){
            if (tree_type[1] == 'a')
               node = search(abtree->avl->top, word);
            else
               node = search(abtree->bst->top, word);
         }
         if (string_length == 0 || node == NULL)
            printf("\"%s\" is not in the tree\n",word);
         else
            printf("the frequency of \"%s\" is %d\n", word, node->frequency_of_word);
      }
      else if (command[0] == 's'){
         breadthFirstTraversal(abtree, "s");
      }
      else{
         breadthFirstTraversal(abtree, "r");
      }
      command = readToken(fp);
   }
   fclose(fp);
}


static char* getTokenOrString(FILE* fp){
   char* temp = "";


   if (stringPending(fp)){
      temp = readString(fp);
      return temp;
   }
   else{
      temp = readToken(fp);
      return temp;

   }
}

static void breadthFirstTraversal(Tree* abtree, char* report_or_show){

   FILE* fp = fopen("output.txt", "w");

   Queue* nodes = initializeQueue();
   int number_of_nodes = 0;
   char* which_tree = "";
   int total_nodes_in_tree = 0;

   int shortest_distance_to_leaf = 0;
   int longest_distance_to_leaf = 0;

   int depth = 0;
   if (abtree->bst->top == NULL && abtree->avl->top == NULL){
      printf("empty tree!\n");
      return;
   }

   if (abtree->bst->top == NULL){
      which_tree = "AVL";
      total_nodes_in_tree = abtree->avl->number_of_nodes;
      shortest_distance_to_leaf = abtree->avl->number_of_nodes;
      enqueue(nodes, abtree->avl->top);
   }
   else{
      which_tree = "BST";
      total_nodes_in_tree = abtree->bst->number_of_nodes;
      enqueue(nodes, abtree->bst->top);
      shortest_distance_to_leaf = abtree->bst->number_of_nodes;
   }

   while (true){


      number_of_nodes = nodes->number_of_elements;

      if (number_of_nodes == 0)
         break;

      if (report_or_show[0] == 's'){
         fprintf(fp,"%d: ", depth);
         //printf("%d: ", depth);
      }
      while (number_of_nodes > 0){
         Node* temp = new_node();
         temp = nodes->bottom;
         if (report_or_show[0] != 's' && (temp->node_value->left == NULL || temp->node_value->right == NULL) && depth < shortest_distance_to_leaf)
            shortest_distance_to_leaf = depth;
         if (report_or_show[0] != 's' && (temp->node_value->left == NULL && temp->node_value->right == NULL) && depth > longest_distance_to_leaf)
            longest_distance_to_leaf = depth;
         if (report_or_show[0] == 's'){
            printNode(temp->node_value, abtree,fp);
            if ((number_of_nodes - 1) != 0)
             //  printf(" ");
            fprintf(fp," ");


         }
         dequeue(nodes);
         if (temp->node_value->left != NULL)
            enqueue(nodes, temp->node_value->left);
         if (temp->node_value->right != NULL)
            enqueue(nodes, temp->node_value->right);
         number_of_nodes--;
      }
      depth++;
      if (report_or_show[0] == 's')
         // printf("\n");
         fprintf(fp,"\n");
   }

   if (report_or_show[0] != 's'){
      printf("The number of nodes in the %s is: %d\n", which_tree, total_nodes_in_tree);
      printf("Distance to closest NULL child: %d\n", shortest_distance_to_leaf);
      printf("Distance to furthest NULL child: %d\n", longest_distance_to_leaf);
   }

}

static void printNode(treeNode* temp, Tree* abtree,FILE* fp){


   char* favorite = "";
   if (temp->left == NULL && temp->right == NULL)
      fprintf(fp, "=");
      //printf("=");
   //printf("%s", temp->value);
   fprintf(fp,"%s", temp->value);

   if (temp->left_height > temp->right_height){
      if (abtree->avl->top != NULL)
         //printf("-");
      fprintf(fp,"-");

      favorite = "-";
   }
   else if (temp->left_height < temp->right_height){
      if (abtree->avl->top != NULL)
         //printf("+");
      fprintf(fp,"+");

      favorite = "+";
   }

   if (temp->parent != NULL)
      printParentStatistics(temp, abtree,fp);

   if (temp->parent == NULL){
      if (abtree->avl->top != NULL)
         //printf("(%s%s)%d", temp->value, favorite, temp->frequency_of_word);
      fprintf(fp,"(%s%s)%d", temp->value, favorite, temp->frequency_of_word);

      else
         //printf("(%s)%d", temp->value, temp->frequency_of_word);
      fprintf(fp,"(%s)%d", temp->value, temp->frequency_of_word);

   }

   if (temp->parent == NULL)
      fprintf(fp,"X");
      //printf("X");
   else if (temp->parent != NULL){
      if (isLeftChild(temp->parent, temp))
         fprintf(fp,"L");
         //printf("L");
      else
       // printf("R");
      fprintf(fp,"R");

   }
}

static void printParentStatistics(treeNode* temp, Tree* abtree, FILE* fp){
   char* favorite = "";
   if (temp->parent->left_height > temp->parent->right_height)
      favorite = "-";
   else if (temp->parent->left_height < temp->parent->right_height)
      favorite = "+";
   if (abtree->avl->top != NULL)
   //   printf("(%s%s)%d", temp->parent->value, favorite, temp->frequency_of_word);
   fprintf(fp,"(%s%s)%d", temp->parent->value, favorite, temp->frequency_of_word);

   else
      fprintf(fp,"(%s)%d", temp->parent->value, temp->frequency_of_word);
      //printf("(%s)%d", temp->parent->value, temp->frequency_of_word);

}

static int isThisAscii(int character){

   if (character <= 0 || character >= 127)
      return false;
   else
      return true;


}