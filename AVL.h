#ifndef AVL_H
#define AVL_H

#include "treeNode.h"

typedef struct AVL{

   int number_of_nodes;
   treeNode* top;

}AVL;


AVL* initializeAVL();

#endif