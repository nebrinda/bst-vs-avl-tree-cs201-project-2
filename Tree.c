//written by nathan brinda
#include "Tree.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


//protoypes for private functions
static treeNode* findPredecessor(treeNode* node);
static void incrementCorrectTree(Tree* abtree, treeNode* top_of_tree);
static treeNode* sibling(treeNode* curr_node);
static void setBalance(treeNode* node_to_set, treeNode* node_to_be_deleted);
static treeNode* getFavorite(treeNode* current_node);
static int isLinear(treeNode* parent, treeNode* insertedNode, treeNode* grandChild);
static void insertionFixup(treeNode* insertedNode, Tree* abtree);
static void rightRotate(treeNode* x, treeNode* y, Tree* abtree);
static void leftRotate(treeNode* x, treeNode* y, Tree* abtree);
static void rotate(treeNode* x, treeNode* y, Tree* abtree);
static int getNodeHeight(treeNode* node);
static void setPointersForDeletion(treeNode* value_to_be_deleted, treeNode* updated_value, Tree* abtree);
static void deleteFixup(treeNode* insertedNode, Tree* abtree);
static void deleteNode(treeNode* is_in, Tree* abtree, treeNode* top_of_tree);
static void decrementCorrectTree(Tree* abtree, treeNode* top_of_tree);


static int counter = 0;

Tree* initializeTree(){

   Tree* abtree = malloc(sizeof(Tree));
   abtree->avl = NULL;
   abtree->bst = NULL;
   return abtree;
}



treeNode* insert(treeNode* top_of_tree, char* value, struct Tree* abtree){
   //no items in bst, add it to the top
   if (top_of_tree == NULL){
      top_of_tree = newTreeNode();
      top_of_tree->value = value;
      top_of_tree->frequency_of_word++;
      return top_of_tree;
   }

   //if the value is smaller check the left child
   if (strcmp(top_of_tree->value, value) > 0){
      if (top_of_tree->left == NULL){
         treeNode* node = newTreeNode();
         node->value = value;
         top_of_tree->left = node;
         top_of_tree->left->frequency_of_word++;
         top_of_tree->left->parent = top_of_tree;
         incrementCorrectTree(abtree, top_of_tree);
         top_of_tree->left->height++;
         if (abtree->bst->top == NULL){
            insertionFixup(top_of_tree->left, abtree);
         }
         return top_of_tree;
      }
      //if the left child exists, recurse left
      else{
         insert(top_of_tree->left, value, abtree);
      }
   }
   //if the value is bigger check the right child
   else if (strcmp(top_of_tree->value, value) < 0){
      if (top_of_tree->right == NULL){
         treeNode* node = newTreeNode();
         node->value = value;
         top_of_tree->right = node;
         top_of_tree->right->frequency_of_word++;
         top_of_tree->right->parent = top_of_tree;
         incrementCorrectTree(abtree, top_of_tree);
         top_of_tree->right->height++;
         if (abtree->bst->top == NULL){
            insertionFixup(top_of_tree->right, abtree);
         }
         return top_of_tree;
      }
      //if the child exists, recurse right
      else{
         insert(top_of_tree->right, value, abtree);
      }
   }
   else
      top_of_tree->frequency_of_word++;

   return NULL;
}

treeNode* delete(treeNode* top_of_tree, char* value_to_delete, Tree* abtree){

   //search to make sure it has the item in the bst
   treeNode* is_in = search(top_of_tree, value_to_delete);
   if (is_in == NULL)
      return NULL;

   if (is_in->frequency_of_word > 1){
      is_in->frequency_of_word--;
      return is_in;
   }

   if (abtree->bst->top != NULL && is_in->left == NULL && is_in->right == NULL){
      //case1: no children
            abtree->bst->number_of_nodes--;
            if (is_in->parent == NULL){//the only node is the root, set root to NULL and return
               abtree->bst->top = NULL;
               return is_in;
            }
            if (isLeftChild(is_in->parent, is_in))
               is_in->parent->left = NULL;
            else
               is_in->parent->right = NULL;
         return is_in;
   }
   else if (abtree->bst->top != NULL && ((is_in->left == NULL && is_in->right != NULL) || (is_in->right == NULL && is_in->left != NULL))){
      if (is_in->left == NULL && is_in->right != NULL){//single right child
         if (is_in->parent == NULL){//deleting the root, when he has a single right child
            abtree->bst->top = is_in->right;
            is_in->right->parent = NULL;
         }
         else if (isLeftChild(is_in->parent, is_in)){//deleting a single node when he is a left child
            is_in->parent->left = is_in->right;
            is_in->right->parent = is_in->parent;
         }
         else{//deleting a single node when he is a right child
            is_in->parent->right = is_in->right;
            is_in->right->parent = is_in->parent;
         }
      }
      else{
         if (is_in->parent == NULL){//deleting the root, when he has a single left child
            abtree->bst->top = is_in->left;
            is_in->left->parent = NULL;
         }
         else if (isLeftChild(is_in->parent, is_in)){//deleting a single node when he is a left child
            is_in->parent->left = is_in->left;
            is_in->right->left = is_in->parent;
         }
         else{//deleting a single node when he is a right child
            is_in->parent->right = is_in->left;
            is_in->left->parent = is_in->parent;
         }
      }
      if (is_in->parent == NULL)
         is_in->parent = NULL;
      abtree->bst->number_of_nodes--;
   }
   //case2: at least 1 child 
   else{

      treeNode* leaf = newTreeNode();

      leaf = findPredecessor(is_in);
      counter = 0;

      setPointersForDeletion(is_in, leaf, abtree);

      leaf->left = NULL;
      leaf->right = NULL;

      //if it's a bst, then just delete the value and don't rotate
      if (abtree->bst->top != NULL)
         return delete(leaf, leaf->value, abtree);

      else{//call deletion fixup and then delete the leaf node
         deleteFixup(leaf, abtree);
         deleteNode(leaf, abtree, top_of_tree);
      }
      return is_in;
   }
   return is_in;
}


static void deleteNode(treeNode* leaf, Tree* abtree, treeNode* top_of_tree){

   decrementCorrectTree(abtree, top_of_tree);
   if (leaf->parent == NULL){//you're the root
      leaf = NULL;
      abtree->avl->top = NULL;
   }
   else if (leaf->parent->left == leaf){//you're a left child
      leaf->parent->left = NULL;
   }
   else{//else right child
      leaf->parent->right = NULL;
   }
   
}


treeNode* search(treeNode* top_of_tree, char* value_to_find){

   //not in the bst
   if (top_of_tree == NULL)
      return NULL;

   //value_to_delete is smaller then top_of_tree
   else if (strcmp(top_of_tree->value, value_to_find) > 0){
      return search(top_of_tree->left, value_to_find);
   }

   //value_to_delete is bigger then top_of_tree
   else if (strcmp(top_of_tree->value, value_to_find) < 0){
      return search(top_of_tree->right, value_to_find);
   }

   //value is equal to node
   else
      return top_of_tree;
}

//replaces the pointers for updated_value with the pointers from value to be deleted has
//swaps the value at value_to_be_deleted with a leaf node value
static void setPointersForDeletion(treeNode* value_to_be_deleted, treeNode* updated_value, Tree* abtree){

   char* temp_value = "";
   int temp_freq = 0;

   temp_value = value_to_be_deleted->value;
   temp_freq = value_to_be_deleted->frequency_of_word;
   value_to_be_deleted->value = updated_value->value;
   value_to_be_deleted->frequency_of_word = updated_value->frequency_of_word;
   updated_value->value = temp_value;
   updated_value->frequency_of_word = temp_freq;
}

//takes in the added node, then performs the necessary rotations 
static void insertionFixup(treeNode* insertedNode, Tree* abtree){
   treeNode* y = newTreeNode();
   treeNode* p = newTreeNode();
   treeNode* temp = newTreeNode();

   while (insertedNode->parent != NULL){
      setBalance(insertedNode->parent, temp);
      if (getNodeHeight(insertedNode) < getNodeHeight(sibling(insertedNode))){
         break;
      }
      if (abs(insertedNode->parent->left_height - insertedNode->parent->right_height) <= 1){
         insertedNode = insertedNode->parent;
      }
      else{
         y = getFavorite(insertedNode); //favorite of inserted node, grandChild
         p = insertedNode->parent; 
         if (y != NULL && y != insertedNode && !isLinear(p, insertedNode, y)){
            rotate(insertedNode, y, abtree);
            rotate(p, y, abtree);
            setBalance(insertedNode, temp);
            setBalance(p, temp);
            setBalance(y, temp);
         }
         else{
            rotate(p, insertedNode, abtree);
            setBalance(p, temp);
            setBalance(insertedNode, temp);
         }
         break;
      }
   }
}

static void deleteFixup(treeNode* insertedNode, Tree* abtree){
   treeNode* node_to_be_deleted = newTreeNode();
   treeNode* p = newTreeNode();
   treeNode* y = newTreeNode();
   treeNode* z = newTreeNode();

   insertedNode->height = 0;
   *node_to_be_deleted = *insertedNode;

   while (insertedNode->parent != NULL){

      if (insertedNode->parent->parent == NULL)
         setBalance(insertedNode->parent, node_to_be_deleted);

      if (getFavorite(insertedNode->parent) == insertedNode){
         setBalance(insertedNode->parent, node_to_be_deleted);
         insertedNode = insertedNode->parent;
      }
      else if (getFavorite(insertedNode->parent) == NULL){
         setBalance(insertedNode->parent, node_to_be_deleted);
         break;
      }
      else{
         p = insertedNode->parent;
         z = sibling(insertedNode);
         y = getFavorite(z);
         if (y != NULL && !isLinear(p, z, y)){
            rotate(z, y, abtree);
            rotate(p, y, abtree);
            setBalance(p, node_to_be_deleted);
            setBalance(z, node_to_be_deleted);
            setBalance(y, node_to_be_deleted);
            insertedNode = y;
         }
         else{
            rotate(p, z, abtree);
            setBalance(p, node_to_be_deleted);
            setBalance(z, node_to_be_deleted);
            if (abs(z->left_height - z->right_height) == 0)
               break;
            insertedNode = z;
         }
      }
   }
}

//rotates y to x (upward) 
static void rotate(treeNode* x, treeNode* y, Tree* abtree){

   if (x->left != NULL && x->right == NULL)
      rightRotate(x, y, abtree);

   else if (x->right != NULL && x->left == NULL)
      leftRotate(x, y, abtree);
   //x is the root
   else if (x->parent == NULL){
      if (strcmp(x->left->value, y->value) == 0)
         rightRotate(x, y, abtree);
      else
         leftRotate(x, y, abtree);
   }
   else{
      if (isLeftChild(y->parent, y))
         rightRotate(x, y, abtree);
      else
         leftRotate(x, y, abtree);

   }

}


//rotates y to x (upward rotation)
static void leftRotate(treeNode* x, treeNode* y, Tree* abtree){

   treeNode* temp_y = newTreeNode();
   *temp_y = *y;

   y->parent = x->parent;
   y->left = x;


   if (x->parent != NULL && x->parent->left != NULL && x->parent->left == x)
      x->parent->left = y;
   else if (x->parent != NULL && x->parent->right != NULL && x->parent->right == x)
      x->parent->right = y;

   x->parent = y;
   x->right = temp_y->left;

   if (x->right != NULL)
      x->right->parent = x;

   if (x == abtree->avl->top)//x was the root
      abtree->avl->top = y;


}


//rotates y to x (upward rotation)
static void rightRotate(treeNode* x, treeNode* y, Tree* abtree){
   treeNode* temp_y = newTreeNode();
   *temp_y = *y;

   y->parent = x->parent;
   y->right = x;

   if (x->parent != NULL && x->parent->left != NULL && x->parent->left == x)
      x->parent->left = y;
   else if (x->parent != NULL && x->parent->right != NULL && x->parent->right == x)
      x->parent->right = y;

   x->parent = y;
   x->left = temp_y->right;
   if (x->left != NULL)
      x->left->parent = x;

   if (x == abtree->avl->top)//x was the root
      abtree->avl->top = y;

}


//returns a slot array, with the first slot representing linearity and the second representing leftness 
static int isLinear(treeNode* parent, treeNode* insertedNode, treeNode* grandChild){

   //is left linear
   if (parent->left != NULL && insertedNode->left != NULL){
      if (isLeftChild(parent, insertedNode) && strcmp(insertedNode->left->value, grandChild->value) == 0)
         return true;
   }
   //is right linear
   if (parent->right != NULL && insertedNode->right != NULL){
      if (!isLeftChild(parent, insertedNode) && strcmp(insertedNode->right->value, grandChild->value) == 0)
         return true;
   }
   //not linear
   return false;
}


//takes in parent and child
int isLeftChild(treeNode* parent, treeNode* insertedNode){
   //you are a left child
   if (parent->left != NULL && strcmp(parent->left->value, insertedNode->value) == 0)
      return true;
   else
      //you are a right child
      return false;
}


//returns the favorite of the node passed to it. (not tested)
static treeNode* getFavorite(treeNode* current_node){
   //no favorite
   if (current_node->left_height == current_node->right_height)
      return NULL;
   //right is favorite
   else if (current_node->left_height < current_node->right_height)
      return current_node->right;
   //left is favorite
   else
      return current_node->left;
}


static void setBalance(treeNode* node_to_set, treeNode* node_to_be_deleted){
   treeNode* temp = node_to_set;
   if (temp->left == NULL && temp->right != NULL){
      temp->left_height = 0;
      temp->right_height = temp->right->height;
      temp->height = (temp->right->height) + 1;

   }

   else if (temp->right == NULL && temp->left != NULL){
      temp->left_height = temp->left->height;
      temp->right_height = 0;
      temp->height = (temp->left->height) + 1;

   }

   else if (temp->right == NULL && temp->left == NULL){
      temp->right_height = 0;
      temp->left_height = 0;
      temp->height = 1;
   }
   else{
      temp->right_height = temp->right->height;
      temp->left_height = temp->left->height;
      temp->height = getNodeHeight(temp);
   }
}


//returns the max of two given integer (not tested)
static int getNodeHeight(treeNode* node){
   return (node->left_height > node->right_height ? node->left_height : node->right_height) + 1;
}


//returns a given nodes sibling, assuming it exists.
static treeNode* sibling(treeNode* curr_node){

   if (curr_node->parent->right == NULL || curr_node->parent->left == NULL)//have no sibling
      return curr_node;
   if (strcmp(curr_node->parent->right->value, curr_node->value) == 0)//you're a right child
      return curr_node->parent->left;
   else
      return curr_node->parent->right;//you're a left child
}


static treeNode* findPredecessor(treeNode* node){
  
   if (counter == 0){
      counter++;//recurse left once
      if (node->left != NULL)
         return findPredecessor(node->left);
   }
   if (node->right == NULL){
      if (node->left == NULL){
         treeNode* temp = newTreeNode(0);
         temp = node;
         return temp;

      }
      else
         return findPredecessor(node->left);
   }
   else
      return findPredecessor(node->right);
}

static void incrementCorrectTree(Tree* abtree, treeNode* top_of_tree){
   if (abtree->avl->top == NULL)
      abtree->bst->number_of_nodes++;
   else
      abtree->avl->number_of_nodes++;
}

static void decrementCorrectTree(Tree* abtree, treeNode* top_of_tree){
   Tree* temp = abtree;
   if (abtree->avl->top == NULL)
      temp->bst->number_of_nodes--;
   else
      abtree->avl->number_of_nodes--;
}
