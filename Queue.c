//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"

Queue* initializeQueue(){
   Queue* queue = malloc(sizeof(Queue));
   queue->number_of_elements = 0;
   queue->bottom = NULL;
   queue->top = NULL;
   return queue;
}

void enqueue(Queue *my_queue, treeNode* value){
   Node* node = new_node();
   node->node_value = value;

   if (my_queue->number_of_elements == 0){
      my_queue->bottom = node;
      my_queue->top = node;
   }
    else{
       my_queue->top->prev = node;
       node->next = my_queue->top;
       my_queue->top = node;
    }
    my_queue->number_of_elements++;
}

Node* dequeue(Queue *my_queue){

   if (my_queue->top == NULL){
      printf("dequeue failed");
      exit(0);
   }

   Node* temp = new_node();
   temp = my_queue->bottom;
   my_queue->bottom = my_queue->bottom->prev;
   my_queue->number_of_elements--;
   return temp;
}
