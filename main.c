//written by nathan brinda

#include <stdio.h>
#include <stdlib.h>

#include "utils.h"
#include "BST.h"
#include "Tree.h"
#include "AVL.h"

int main(int argc, char** argv){

   AVL* my_avl = initializeAVL();
   BST* my_bst = initializeBST();
   Tree* abtree = initializeTree();
   abtree->avl = my_avl;
   abtree->bst = my_bst;
   
   FILE* fp;

   if (argc < 3){
      printf("must supply 3 command line arguments");
      exit(0);   
   }

      fp = fopen(argv[2], "r");
      process_text(fp, abtree, argv[1]);
  
      fp = fopen(argv[3], "r");
      interpreter(fp, abtree, argv[1]);

   return 0;
}


