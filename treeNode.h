//written by nathan brinda
#ifndef TREENODE_H
#define TREENODE_H

typedef struct treeNode{

   struct treeNode* parent;
   struct treeNode* left;
   struct treeNode* right;
   int frequency_of_word;
   int left_height;
   int right_height;
   int height;
   char* value;

}treeNode;

treeNode* newTreeNode();

#endif